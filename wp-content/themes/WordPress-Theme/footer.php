		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( ! is_page( array( 'quienes-somos' ) ) ) : get_template_part( 'part', 'block-2' ); endif; ?>
		<?php if ( ! is_page( array( 'contactenos' ) ) ) : get_template_part( 'part', 'block-3' ); endif; ?>
		<?php get_template_part( 'part', 'block-4' ); ?>
		<?php get_template_part( 'part', 'block-5' ); ?>
		<?php get_template_part( 'part', 'block-6' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>