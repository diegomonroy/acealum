<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->